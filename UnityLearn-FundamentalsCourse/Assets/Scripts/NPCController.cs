﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    [SerializeField] float patrolTime = 10f;
    [SerializeField] float patrolSpeed = 5f;
    [SerializeField] float aggroRange;
    [SerializeField] Transform[] waypoint;
    int index;
    float speed, agentSpeed;
    Transform player;

    //Animator anim;
    NavMeshAgent agent;

    void Awake()
    {
        //anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        if (agent != null)
        {
            agentSpeed = agent.speed;
        }
        player = GameObject.FindGameObjectWithTag("Player").transform;
        index = Random.Range(0, waypoint.Length);

        InvokeRepeating("Tick",0, 0.5f);

        if (waypoint.Length > 0)
        {   
            InvokeRepeating("Patrol", 0, patrolTime);
        }
    }

    void Patrol()
    {
        index = index == waypoint.Length - 1 ? 0 : index + 1;
    }

    void Tick()
    {
        agent.destination = waypoint[index].position;
        agent.speed = agentSpeed / 2;
        if (player != null && Vector3.Distance(this.transform.position, player.position) < aggroRange)
        {
            agent.destination = player.position;
            agent.speed = agentSpeed;
            Debug.Log("Got player in range" + Vector3.Distance(transform.position, player.position));
        }
        else
        {
            Debug.Log("Nope");
        }
    }
}
