﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseManager : MonoBehaviour
{
    public LayerMask clickableLayer;

    public Texture2D pointer;
    public Texture2D targetPointer;
    public Texture2D doorwayPointer;
    public Texture2D combatPointer;

    public EventVector3 OnClickEnviroment;


    // Update is called once per frame
    void Update()
    {
        RaycastHit rayHit;
        // why you didn't use switch case ? it's same and less code. Stupid tutor
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, 50, clickableLayer))
        {
            bool door = false;
            bool item = false;

            string hitObjectTag = rayHit.collider.gameObject.tag;

            switch (hitObjectTag)
            {
                case "Doorway":
                    Cursor.SetCursor(doorwayPointer, new Vector2(16, 16), CursorMode.Auto);
                    door = true;
                    break;
                case "Enemy":
                    Cursor.SetCursor(combatPointer, new Vector2(16, 16), CursorMode.Auto);
                    break;
                case "Item":
                    Cursor.SetCursor(pointer, new Vector2(16, 16), CursorMode.Auto);
                    item = true;
                    break;
                default:
                    Cursor.SetCursor(targetPointer, new Vector2(16, 16), CursorMode.Auto);
                    door = false;
                    break;

            }

            if (Input.GetMouseButtonDown(0))
            {
                if (door)
                {
                    OnClickEnviroment.Invoke(rayHit.collider.transform.position);
                    Debug.Log("Moving to door");
                }
                else if (item)
                {
                    OnClickEnviroment.Invoke(rayHit.collider.transform.position);
                    Debug.Log("Moving to item");
                }
                else
                {
                    OnClickEnviroment.Invoke(rayHit.point);
                }
            }
        }
        else
        { Cursor.SetCursor(pointer, Vector2.zero, CursorMode.Auto); }
    }
}

[System.Serializable]
public class EventVector3 : UnityEvent<Vector3> { }